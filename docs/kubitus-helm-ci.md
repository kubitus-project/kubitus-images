# `kubitus-helm-ci`

- For each included chart (in `charts/*` directories):
    - `charts/<chart>/values.schema.json` should exists
    - `charts/<chart>/values.schema.json` should be strict (using `additionalProperties=false` for example)
    - An empty `charts/<chart>/ci/empty-values.yaml` file is created
    - For each `charts/<chart>/ci/<values>-values.yaml`:
        - the chart is tested against this values file, using `helm template` and `helm lint`
        - if a corresponding golden file is found (`charts/<chart>/ci/<values>-golden.yaml`), the result should exactly match
    - [`helm-docs`](https://github.com/norwoodj/helm-docs) is called, and the generated `README.md` should match the version in the Git repository.
- Files in directory `charts` matching pattern `*.tgz` are forbidden.
- a list of images `images.list` is created by extracting `image` from computed resources and from `ci/*.gitlab-ci.yml`
- a list of image repositories `image-repositories.list` is created from `images.list`. This file should match the version in the Git repository
- Both generated `images.list` and `image-repositories.list` should match commited versions
- for each `patches/*.patch`, patch is checked to be already applied (with `--strip=1`)
- `yamllint` is called
