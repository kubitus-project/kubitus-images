# `kubitus-sync-images`

This script uses GitLabracadabra to sync images to a GitLab instance.

The script expects a list of images (one per line) in a file named `images.list` (created from [`kubitus-helm-ci`](kubitus-helm-ci.md)).

It accepts the following environment variables :

| Environment variable | Description | Default value |
| -------------------- | ----------- | ------------- |
| `SYNC_TRIVY_DB` | Also sync [`public.ecr.aws/aquasecurity/trivy-db:2`](https://github.com/aquasecurity/trivy-db/pkgs/container/trivy-db) image | `true` on first job (or no job), `false` otherwise |
| `SYNC_TRIVY_JAVA_DB` | Also sync [`public.ecr.aws/aquasecurity/trivy-java-db:1`](https://github.com/aquasecurity/trivy-java-db/pkgs/container/trivy-java-db) image | `${DEFAULT_SYNC_TRIVY_JAVA_DB:-false}` on first job (or no job), `false` otherwise |
| `SYNC_TRIVY_CHECKS` | Also sync [`public.ecr.aws/aquasecurity/trivy-checks:0`](https://github.com/aquasecurity/trivy-checks/pkgs/container/trivy-checks) image | `true` on first job (or no job), `false` otherwise |
| `ENV_FILE` | Environement variables, like `HTTP_PROXY=http://squid:3128` | `''` |
| `GITLAB_URL` | GitLab URL | `$CI_SERVER_URL` |
| `GITLAB_PRIVATE_TOKEN` | GitLab private token | `$CI_JOB_TOKEN` |
| `EXTERNAL_REGISTRIES_GROUP` | External registries group | `external-registries` |
| `SSH_DESTINATION` | When not empty, SSH to this host | `''` |
| `SSH_KNOWN_HOSTS_FILE` | Add to `~/.ssh/known_hosts` | `''` |
| `SSH_PRIVATE_KEY_FILE` | Loaded with `ssh-add` to `ssh-agent` | `''` |
| `USE_SUDO` | Use SUDO | `false` |
| `USE_DOCKER` | Use docker | `false` |
| `CONTAINER_NAME` | Container name | `gitlabracadabra-$CI_COMMIT_SHA` |
| `GITLABRACADABRA_IMAGE` | GitLabracadabra image | `registry.gitlab.com/gitlabracadabra/gitlabracadabra:...` |
