# kubitus-images

## Purpose

`Dockerfile`s for images used in `kubitus-installer`.

## Images

One flavor is available:

- `helm-ci`
  - Based on `debian:bookworm`
  - Including packages: `ca-certificates`, `curl`, `git` and `openssh-client`
  - Including binary: `helm`
  - Including helper scripts: [`kubitus-helm-ci`](docs/kubitus-helm-ci.md) and [`kubitus-sync-images`](docs/kubitus-sync-images.md)
  - Creating group and user `helm-ci`
  - Running as `helm-ci` user


The image name is `registry.gitlab.com/kubitus-project/kubitus-images/${flavor}`.
For example, to retrieve the latest `helm-ci` image:
`registry.gitlab.com/kubitus-project/kubitus-images/helm-ci`.
